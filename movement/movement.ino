// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            6

// How many NeoPixels are attached to the Arduino?
#define REPEATER_PIXEL_COUNT 2
#define NUMPIXELS      (144 + REPEATER_PIXEL_COUNT)

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


void setup() {
  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
//#if defined (__AVR_ATtiny85__)
//  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
//#endif
  // End of trinket special code

  pixels.begin(); // This initializes the NeoPixel library.

  Serial.begin(57600);

  for (int i=0; i<REPEATER_PIXEL_COUNT; i++) {
    pixels.setPixelColor(i, pixels.Color(0, 0, 0));
  }
  pixels.show();

}

int sat = 5;


float pulseRadius = 0.01f;
float PULSE_START = 1.0 + pulseRadius*2;
float PULSE_END = -0.5f;
float DEFAULT_PULSE_SPEED = -0.01f;


const byte MAX_PULSES = 2;
float pulsePos[MAX_PULSES] = { PULSE_START, PULSE_START };//, PULSE_START, PULSE_START };
float pulseSpeed[MAX_PULSES] = { -0.025, 0 };//, 0, 0 };
byte pulseHue[MAX_PULSES] = { 0, 0 };//, 0, 0 };
int delayMs = 0; 

bool doUpdatePulse = true;
bool doAttractMode = true;

bool inMessage = false;


void loop() {

  requestSerial();

  if (doUpdatePulse) {
    //Serial.write(0);
    updatePulse();
    //Serial.write(1);
    //delay(delayMs);
  }

  
}

void updatePulse() {

  for (int j=0; j<MAX_PULSES; j++) {
    pulsePos[j] += pulseSpeed[j];
    //Serial.print(j);
    //Serial.print(": ");
    //Serial.print(pulsePos[j], 3);
    //Serial.print(", ");
    if ((pulsePos[j] < (-pulseRadius*2.0f)) || (pulsePos[j] > (1.0f + pulseRadius*2.0f))) {
      if (doAttractMode) {
        pulsePos[j] = PULSE_START;
      } else {
        pulseSpeed[j] = 0;
      }
    }
  }ls
  //Serial.println();

  for(int i=0;i<NUMPIXELS;i++){
    int combinedRgb[3] = { 0, 0, 0 };
    int rgb[3];
    for (int j=0; j<MAX_PULSES; j++) {
      int val = getBrightnessForPosition(i, pulsePos[j]);
      if (val > 0) {
        getRGB(pulseHue[j], sat, val, rgb);
        combinedRgb[0] += rgb[0];
        combinedRgb[1] += rgb[1];
        combinedRgb[2] += rgb[2];
      }
    }
    combinedRgb[0] = min(combinedRgb[0], 255);
    combinedRgb[1] = min(combinedRgb[1], 255);
    combinedRgb[2] = min(combinedRgb[2], 255);

    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    
    pixels.setPixelColor(i+REPEATER_PIXEL_COUNT, pixels.Color(combinedRgb[0],combinedRgb[1],combinedRgb[2])); 

  }
  pixels.show(); // This sends the updated pixel color to the hardware.
}


const byte MESSAGE_LENGTH = 5;
const byte START_MARKER_LENGTH = 2;

const byte REQUEST_DATA_BYTE = 254;
const byte ACK_DATA_FAIL = 128;
const byte ACK_DATA_SUCCESS = 127;

const byte MESSAGE_SET_HUES = 0x01;
const byte MESSAGE_START_PULSE = 0x02;

byte serialBuffer[MESSAGE_LENGTH];
int serialBufferPos = 0;
int startMarkerPos = 0;
// message start: 0xfe 0xfe

void requestSerial() {
  while (Serial.available()) {
    Serial.read(); // flush
  }
  
  Serial.write(REQUEST_DATA_BYTE);
  byte header[2];
  byte bytesRead = Serial.readBytes(header, 2);
  if (bytesRead == 0) {
    return;
  }
  
  if (header[0] == 0xfe && header[1] == 0xfe) {
    Serial.readBytes(serialBuffer, MESSAGE_LENGTH);
    Serial.write(ACK_DATA_SUCCESS);
    processMessage();
  } else if (header[0] == 0x00 && header[1] == 0x00) {
    Serial.write(ACK_DATA_SUCCESS);
  } else {
    Serial.write(ACK_DATA_FAIL);
  }
}

void decodeSerial() {
  while (Serial.available()) {
    byte b = Serial.read();
    //Serial.write(b);
    if (inMessage) {
      serialBuffer[serialBufferPos] = b;
      ++serialBufferPos;
      if (serialBufferPos == MESSAGE_LENGTH) {
        Serial.write(255);
        Serial.write(255);
        processMessage();
        inMessage = false;
        startMarkerPos = 0;
      }
    } else {
      if (b == 0xfe) {
        ++startMarkerPos;
        if (startMarkerPos == START_MARKER_LENGTH) {
          inMessage = true;
          serialBufferPos = 0;
          continue;
        }
      } else {
        Serial.write(0);
        startMarkerPos = 0;
      }
    }
  }
}

void processMessage() {

  doAttractMode = false;

  doUpdatePulse = true;

  for (int i=0; i<MESSAGE_LENGTH; i++) {
    Serial.write(serialBuffer[i]);
  }
    

  if (serialBuffer[0] == MESSAGE_SET_HUES) {
    doUpdatePulse = false;
    // hue values for each led
    for (int i=0; i<4; i++) {
  
      int hue = (int)(serialBuffer[i+1] * (360.0f/255.0f));
      int val = 255;
      int rgb[3];
      getRGB(hue, sat, val, rgb);
  
      // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
      
      pixels.setPixelColor(i+REPEATER_PIXEL_COUNT, pixels.Color(rgb[0],rgb[1],rgb[2])); 
    }
    pixels.show(); // This sends the updated pixel color to the hardware.
  } else if (serialBuffer[0] == MESSAGE_START_PULSE) {

    int pulseIndex = -1;
    for (int j=0; j<MAX_PULSES; j++) {
      if (pulseSpeed[j] == 0) {
        pulseIndex = j;
      }
    }
    
    if (pulseIndex == -1) {
      // steal at random
      pulseIndex = random(MAX_PULSES);
      /*
      // find the least advanced pulse
      float bestPos = pulsePos[0];
      int bestIndex = 0;
      for (int j=1; j<MAX_PULSES; j++) {
        if (pulsePos[j] < bestPos) {
          bestIndex = j;
          bestPos = pulsePos[j];
        }
      }
      pulseIndex = bestIndex;*/
    }
    
    //pulseSpeed = DEFAULT_PULSE_SPEED;
    byte pulseSpeedHi = serialBuffer[1];
    byte pulseSpeedLo = serialBuffer[2];
    pulseSpeed[pulseIndex] = -((float)((pulseSpeedHi * 256) + pulseSpeedLo) / 8192.0f);
    
    byte pulseRadiusByte = serialBuffer[3];
    pulseRadius = (float)pulseRadiusByte / 512.0f;

    pulsePos[pulseIndex] = 1.0 + pulseRadius*2;

    byte pulseHueByte = serialBuffer[4];
    pulseHue[pulseIndex] = pulseHueByte;

    
    
    //pulseSpeed = (float)((serialBuffer[1] * 255) + serialBuffer[2]) / 1024.0f;
  }
}



//float positions[NUMPIXELS] = { 0, 0.3333f, 0.6667f, 1.0f };


float getPositionForPixel(int whichPixel) {

  const float LENGTH = 1.0f;
  const float LENGTH_PER_PIXEL = LENGTH / (NUMPIXELS-1);
  return LENGTH_PER_PIXEL * whichPixel;
}

byte getBrightnessForPosition(int whichPixel, float pulsePos)
{
  float delta = getPositionForPixel(whichPixel) - pulsePos;
  float deltaNorm = delta / pulseRadius;
  float deltaNormSq = deltaNorm * deltaNorm;
  if (deltaNormSq > 1.0f) {
    return 0;
  }
  deltaNormSq = 1.0f - deltaNormSq;
  //deltaNormSq *= deltaNormSq;

  return (byte)(deltaNormSq * 255.0f);
}





/* 
  dim_curve 'lookup table' to compensate for the nonlinearity of human vision.
  Used in the getRGB function on saturation and brightness to make 'dimming' look more natural. 
  Exponential function used to create values below : 
  x from 0 - 255 : y = round(pow( 2.0, x+64/40.0) - 1)   
*/

const byte dim_curve[] = {
    0,   1,   1,   2,   2,   2,   2,   2,   2,   3,   3,   3,   3,   3,   3,   3,
    3,   3,   3,   3,   3,   3,   3,   4,   4,   4,   4,   4,   4,   4,   4,   4,
    4,   4,   4,   5,   5,   5,   5,   5,   5,   5,   5,   5,   5,   6,   6,   6,
    6,   6,   6,   6,   6,   7,   7,   7,   7,   7,   7,   7,   8,   8,   8,   8,
    8,   8,   9,   9,   9,   9,   9,   9,   10,  10,  10,  10,  10,  11,  11,  11,
    11,  11,  12,  12,  12,  12,  12,  13,  13,  13,  13,  14,  14,  14,  14,  15,
    15,  15,  16,  16,  16,  16,  17,  17,  17,  18,  18,  18,  19,  19,  19,  20,
    20,  20,  21,  21,  22,  22,  22,  23,  23,  24,  24,  25,  25,  25,  26,  26,
    27,  27,  28,  28,  29,  29,  30,  30,  31,  32,  32,  33,  33,  34,  35,  35,
    36,  36,  37,  38,  38,  39,  40,  40,  41,  42,  43,  43,  44,  45,  46,  47,
    48,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,
    63,  64,  65,  66,  68,  69,  70,  71,  73,  74,  75,  76,  78,  79,  81,  82,
    83,  85,  86,  88,  90,  91,  93,  94,  96,  98,  99,  101, 103, 105, 107, 109,
    110, 112, 114, 116, 118, 121, 123, 125, 127, 129, 132, 134, 136, 139, 141, 144,
    146, 149, 151, 154, 157, 159, 162, 165, 168, 171, 174, 177, 180, 183, 186, 190,
    193, 196, 200, 203, 207, 211, 214, 218, 222, 226, 230, 234, 238, 242, 248, 255,
};


// hue = 0..360, others = 0..255

void getRGB(int hue, int sat, int val, int colors[3]) { 
  /* convert hue, saturation and brightness ( HSB/HSV ) to RGB
     The dim_curve is used only on brightness/value and on saturation (inverted).
     This looks the most natural.      
  */

  val = dim_curve[val];
  sat = 255-dim_curve[255-sat];

  int r;
  int g;
  int b;
  int base;

  if (sat == 0) { // Acromatic color (gray). Hue doesn't mind.
    colors[0]=val;
    colors[1]=val;
    colors[2]=val;  
  } else  { 

    base = ((255 - sat) * val)>>8;

    switch(hue/60) {
    case 0:
        r = val;
        g = (((val-base)*hue)/60)+base;
        b = base;
    break;

    case 1:
        r = (((val-base)*(60-(hue%60)))/60)+base;
        g = val;
        b = base;
    break;

    case 2:
        r = base;
        g = val;
        b = (((val-base)*(hue%60))/60)+base;
    break;

    case 3:
        r = base;
        g = (((val-base)*(60-(hue%60)))/60)+base;
        b = val;
    break;

    case 4:
        r = (((val-base)*(hue%60))/60)+base;
        g = base;
        b = val;
    break;

    case 5:
        r = val;
        g = base;
        b = (((val-base)*(60-(hue%60)))/60)+base;
    break;
    }

    colors[0]=r;
    colors[1]=g;
    colors[2]=b; 
  }   
}


